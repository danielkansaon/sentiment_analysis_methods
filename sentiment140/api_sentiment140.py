import sys
from subprocess import call, check_output, check_call
import os
from threading import Thread
from time import sleep
from glob import glob
import json
import requests
import urllib2
import time
import BeautifulSoup

root = "/home/matheus/Datasets-Methods/benchmark_paper/All_Labeled_Datasets/English_Datasets/datasets_paper_benchmark"
datasets_pos_neg = root + "/pos_neg_datasets/"
datasets_raw = root + "/raw_datasets/"

raw_files = glob(datasets_raw + "*sem_score.txt")
pos_neg_files = glob(datasets_pos_neg + "*sem_score.txt")

all_datasets_names = [
			"rawposts",
			"debate",
			"english_dailabor",
			"irony",
			"nikolaos_ted",
			"pang_movie",
			"sarcasm",
			"sentistrength_bbc",
			"sentistrength_digg",
			"sentistrength_myspace",
			"sentistrength_rw",
			"sentistrength_twitter",
			"sentistrength_youtube",
			"stanford_tweets",
			"vader_amazon",
			"vader_movie",
			"vader_nyt",
			"vader_twitter",
			"yelp_reviews",
			"tweet_semevaltest",
			"aisopos_ntua",
			"sanders"
			]

def send(api_input):
	response = urllib2.urlopen('http://www.sentiment140.com/api/bulkClassifyJson', api_input)
	try:
		text = response.read()
		data = json.loads(BeautifulSoup.BeautifulSoup(text).text)
	except:
		try:
			data = json.loads(text.encode("utf8","ignore"))
		except:
			import ipdb;ipdb.set_trace()
	return data

def write_output(output,output_file):
	out_file = open(output_file,"w")
	for line in output:
		out_file.write(str(line["id"]) + "\t" + str(line["polarity"]) + "\n")

	


def createInput(chunk):
	json_data = {"language":"en","data":chunk}
	return json.dumps(json_data)

def run_file_sentiment140_api(file_name,Neutral=True):
	file_pointer = open(file_name,"r")
	output_folder = "/home/matheus/Datasets-Methods/benchmark_paper/results/raw_outputs"
	for dataset in all_datasets_names:
		if dataset in file_name:
			output_file = output_folder + "/apisentiment140." + dataset + "." + str(Neutral)
	#Create Chunks
	chunks = []
	i = 0

	output = []

	for line in file_pointer.readlines():
		line = line.replace("\n","")
		if i%3000 == 0:
			chunks.append([])
		chunks[-1].append({"text":line,"id":i})
		i=i+1
	#Run Sentiment140 in chunks
	i = 0
	for chunk in chunks:
		api_input = createInput(chunk)
		result = send(api_input)

		for line in result["data"]:
			output.append(line)
		if i >= 2:
			time.sleep(60)
		i+=1

	write_output(output,output_file)

for f in raw_files:
	print f,True
	run_file_sentiment140_api(f,True)
for f in pos_neg_files:
	print f,False
	run_file_sentiment140_api(f,False)



