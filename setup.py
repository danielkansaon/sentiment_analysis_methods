import os
import subprocess

#This file install packages in Python2(2.7.17) and Python3 (3.6.9)

print """
    You should install these packages equivalent:
    sudo apt-get install python-setuptools
    sudo apt-get install build-essential
    sudo apt-get install python-dev
    sudo apt-get install libxslt1-dev
    sudo apt-get install libxml2-dev
    sudo pip2 install virtualenv
    sudo apt-get install openjdk-7
    """


from setuptools import setup, find_packages

#Python2

print "Install Perl dependencies(local/lib.pm):"
cmd = ['sudo', 'cpan', 'local/lib.pm']
subprocess.call(cmd)
print "Install Perl dependencies(Lingua/StopWords.pm):"
cmd = ['sudo', 'cpan', 'Lingua/StopWords.pm']
subprocess.call(cmd)
print "Install Perl dependencies(HTML/Filter.pm):"
cmd = ['sudo', 'cpan', 'HTML/Filter.pm']
subprocess.call(cmd)
print "Install Numpy in this Python:"
cmd = ['pip2', 'install', 'numpy']
subprocess.call(cmd)
print "Install specific setuptools in this Python:"
cmd = ['pip2', 'install', 'setuptools==44.0.0']
subprocess.call(cmd)
print "Install NLTK 3.4.5 in this Python:"
cmd = ['pip2', 'install', 'nltk==3.4.5']
subprocess.call(cmd)
print "Install sasa in this Python:"
cmd = ['pip2', 'install', 'sasa==0.2.0']
subprocess.call(cmd)
print "Install pattern in this Python:"
cmd = ['pip2', 'install', 'pattern==2.6']
subprocess.call(cmd)

import importlib
nltk = importlib.import_module('nltk')

nltk.download('stopwords')
nltk.download('maxent_treebank_pos_tagger')
nltk.download('punkt')
#nltk.download('universal_tagset')


requires = [
    'nltk==3.4.5',
    'sasa==0.2.0',
    "vaderSentiment==3.3.1",
    ]

#Python3
print "Install vader in Python3:"
cmd = ['pip3', 'install', 'vaderSentiment==3.3.1']
subprocess.call(cmd)



