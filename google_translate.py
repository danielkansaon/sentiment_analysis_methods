# -*- coding: utf-8 -*-

from googletrans import Translator
import re
import json
import platform
import codecs
import time 
import traceback
import pandas as pd

def translate_text(text):

    translator = Translator()
    print("text: ", text)
    result = translator.translate(text, dest="en").text
    print('result: ', result)
    return result

def start_process(filename):
    intern_data = pd.read_csv('data/'+filename, sep=r'\t', header=0, engine='python')
    target = intern_data['class']    
    text = intern_data['text']
    count = 0

    f = open('data/google_' + filename, "a+", encoding='utf-8')
    f.seek(0)
    start_pos = len(f.readlines())
    f.seek(start_pos)
    
    for i in range(start_pos, len(text)):
        f.write(translate_text(text[i]) + '\t' + str(target[i]) + '\n')
        count += 1
        print(count)

    f.close()

def main():                
    #start_process('dados_internos.tsv')
    start_process('dados_externos.tsv')

# if __name__ == '__main__':
#     main()
