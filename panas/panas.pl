#!/usr/bin/perl -w
	#use warnings;
	use HTML::Filter;
	use HTML::Entities;
	use Lingua::StopWords qw( getStopWords );
	use List::Util qw/max/;
	
	#&abreArqs;
	my $arg1; #-t ou -f
	my $arg2; #tweet ou arquivo
	my $c = 0;
	our $text;
	
	our $stopwords = getStopWords('en');
	our %sentimentos_results = ("attentiveness"=>0, "fatigue"=>0, "fear"=>0, "guilt"=>0, "hostility"=>0, "joviality"=>0, "sadness"=>0, "assurance"=>0, "serene"=>0, "shyness"=>0, "surprise"=>0);
	
	our @positive = ("attentiveness", "joviality", "assurance", "serene", "surprise");
	our @negative = ("fatigue", "fear", "guilt", "hostility", "sadness", "shyness");	
	
	our $pos = 1;
	our $neg = -1;
	our $neut = 0;
	
	our $empate_total = 0;
	#our $total = 0;
	our $texthtml;
	our $datet;
	our $contpos = 0;
	our $contneg = 0;
	our $contneut = 0;
	
	#extrai informacoes dos parametros passados na linha de comando
	foreach $argnum (0 .. $#ARGV) 
	{
		if($c == 0)
		{
			$arg1 = $ARGV[$argnum];
			$c+=1;
		}
		elsif($c == 1)
		{
			$arg2 = $ARGV[$argnum];
			$c+=1;
		}
		#print "$ARGV[$argnum]\n";
	}
	
	if($arg1 eq "-t")
	{
		$text = $arg2;
		&trataTweet;
	}
	elsif($arg1 eq "-f")
	{
		&abreArqs;
	}

	#our $conttweet=0;
	
	#while (my $line = <IN>) 
	#{
		#chomp $line; #apaga um caractere de nova linha final(/n) do t�rmino de linha lida
		
		#if( $line =~ /(\S+) (\S+) (\S+) (\S+) <d>([^<]+)<\/d> <s>([^<]+)<\/s> <t>([^<]+)<\/t> (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) <n>([^<]+)<\/n> <ud>([^<]+)<\/ud> <t>([^<]+)<\/t> <l>([^<]+)<\/l>/)
		#{		
			#$text = $7;
			#$conttweet+=1;
			
			#if($line =~ /\bi'm\b/i or $line =~ /\bi am\b/i or $line =~ /\bfeelin/i or $line =~ /\bim\b/i or $line =~ /\biam\b/i or $line =~ /\bi'm\b/i) 
			#{
				#$text = $line;
				#&trataTweet;
				#$total+=1;
			#}	
		#}
	#}
	
	sub abreArqs
	{
		#my $datalog = "/scratch1/datasets/twitter/logs-final";
		#my $dirlog = "/home/pollyanna/Projeto/PANAS/Nova_Saida";
		#my $event = "tweets-final.txt.gz";
		#my $saida = "saida_panas.txt.gz";
		
		open(IN, "$arg2") || die "Could not open $arg2\n";
		#open(OUT, "| gzip -c > $dirlog/$saida") || die "Cannot create $dirlog/$saida\n";
		
		while (my $line = <IN>) 
		{
			chomp $line; #apaga um caractere de nova linha final(/n) do t�rmino de linha lida
			
			#if( $line =~ /(\S+) (\S+) (\S+) (\S+) <d>([^<]+)<\/d> <s>([^<]+)<\/s> <t>([^<]+)<\/t> (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) <n>([^<]+)<\/n> <ud>([^<]+)<\/ud> <t>([^<]+)<\/t> <l>([^<]+)<\/l>/)
			#{		
				#$text = $7;
				#$conttweet+=1;
				
				#if($line =~ /\bi'm\b/i or $line =~ /\bi am\b/i or $line =~ /\bfeelin/i or $line =~ /\bim\b/i or $line =~ /\biam\b/i or $line =~ /\bi'm\b/i) 
				#{
					$text = $line;
					&trataTweet;
					#$total+=1;
				#}	
			#}
		}
		
	}
	
	sub trataTweet
	{	
			$texthtml = decode_entities($text);	#replaces HTML entities found in the $string with the corresponding ISO-8859/1. (Unrecognized entities are left alone)
			$texthtml =~ s/(www|http|https|ftp|gopher|telnet|file|notes|ms-help)(\S+)+/ /g; #elimina www.(qualquercoisa)
			$texthtml =~ s/\bRT\b|&+|:+|\.+|,+|;+|\*|\!|>|<|\?|\=|\[|\]|\)|\(| |_|-|\/+/ /g;
			$texthtml =~ s/@([0-9]*[A-Za-z_]+[0-9]*)//g; #retira @users
			$texthtml =~ s/%([0-9]*[A-Za-z_]+[0-9]*)//g; #retira %...
			
			my @words_v = split(" ", $texthtml); #separa palavras do tweet
			my @words_v2 = grep { !$stopwords->{$_} } @words_v; #remove stopwords
			
			my %empate = ();
			my %s_count = ("attentiveness"=>0, "fatigue"=>0, "fear"=>0, "guilt"=>0, "hostility"=>0, "joviality"=>0, "sadness"=>0, "assurance"=>0, "serene"=>0, "shyness"=>0, "surprise"=>0);
			
			foreach(@words_v2)
			{	#calcula total dos sentimentos encontrados no tweet
				if($_ =~ /\balert/i or $_ =~ /\battentive/i or $_ =~ /\bconcentrat/i or $_ =~ /\bdetermined/i)
				{
					$s_count{"attentiveness"}+=1;
				}
				elsif($_ =~ /\bsleep/i or $_ =~ /\btired\b/i or $_ =~ /\bsluggish/i or $_ =~ /\bdrowsy\b/i or $_ =~ /\bdrowsi/i)
				{
					$s_count{"fatigue"}+=1;
				}
				elsif($_ =~ /\bafraid\b/i or $_ =~ /\bscare/i or $_ =~ /\bscari/i or $_ =~ /\bscary\b/i or $_ =~ /\bjittery\b/i or $_ =~ /\bshaki/i or $_ =~ /\bshaky\b/i or $_ =~ /\bnerv/i)
				{
					$s_count{"fear"}+=1;
				}
				elsif($_ =~ /\bguilt/i or $_ =~ /\bashame/i or $_ =~ /\bshame\b/i or $_ =~ /\bblameworthy\b/i or $_ =~ /\bdissatisfi/i or $_ =~ /\bdisgust/i)
				{
					$s_count{"guilt"}+=1;
				}
				elsif($_ =~ /\bangry\b/i or $_ =~ /\banger\b/i or $_ =~ /\bangri/i or $_ =~ /\bhostile/i or $_ =~ /\birrit/i or $_ =~ /\bscorn/i or $_ =~ /\bdisgust/i  or $_ =~ /\bloath/i)
				{
					$s_count{"hostility"}+=1;
				}
				elsif($_ =~ /\bhappy\b/i or $_ =~ /\bjoyful/i or $_ =~ /\bdelight/i or $_ =~ /\bcheerful/i or $_ =~ /\bexcit/i or $_ =~ /\benthusiast/i or $_ =~ /\blivel/i  or $_ =~ /\benergetic/i)
				{
					$s_count{"joviality"}+=1;
				}
				elsif($_ =~ /\bsad/i or $_ =~ /\bdownhearted\b/i or $_ =~ /\balone\b/i or $_ =~ /\bloneli/i)
				{
					$s_count{"sadness"}+=1;
				}
				elsif($_ =~ /\bproud\b/i or $_ =~ /\bstrong/i or $_ =~ /\bdare\b/i or $_ =~ /\bdaring/i)
				{
					$s_count{"assurance"}+=1;
				}
				elsif($_ =~ /\bcalm/i or $_ =~ /\brelax/i)
				{
					$s_count{"serene"}+=1;
				}
				elsif($_ =~ /\bshy/i or $_ =~ /\bbashful/i or $_ =~ /\bsheepish/i or $_ =~ /\btimid/i)
				{
					$s_count{"shyness"}+=1;
				}
				elsif($_ =~ /\bamazing\b/i or $_ =~ /\bsurpris/i or $_ =~ /\bastonish/i or $_ =~ /\bamazed/i or $_ =~ /\bamaze/i)
				{
					$s_count{"surprise"}+=1;
				}
			
			}#VERIFICAR SE A CHAVE DEVE SER FECHADA OU NAO

				my $max_sentiment = max values %s_count; #retorna o maior valor encontrado na hash
				my %s_count_aux = reverse %s_count; #para me dar a chave pelo valor passado
				my $key = $s_count_aux{$max_sentiment}; #retorna a chave do valor 
				
				if($s_count{$key} != 0) #verifica se chave nao tem valor zero
				{
					$empate{$key} = $key; #adiciona chave na hash empate
					
					foreach my $k (keys %s_count) #verifica se houve empates, caso afirmativo adicionar na hash empate
					{
						if($k ne $key) 
						{
							if($s_count{$k} == $s_count{$key})#valor encontrado eh igual maior valor da hash
							{
								$empate{$k} = $k;
							}
						}
					}
					
					if(!(!keys %empate)) #se existe chaves na hash de empate
					{
						foreach my $sent (keys %empate)
						{
							my $aux = 0;
							#$sentimentos_results{$sent}+=1;
							foreach(@positive)
							{								
								if($_ eq $sent)
								{
									$contpos+= 1;
									$aux+=1;
								}
							}
							foreach(@negative)
							{
								if($_ eq $sent)
								{
									$contneg+= 1;
									$aux+=1;
								}
							}
							if($aux == 0)
							{
								$contneut+=1;
							}
						}
						if(($contpos > $contneg) && ($contpos > $contneut))
						{
							print "$pos\n"; 
						}
						elsif(($contneg > $contpos) && ($contneg > $contneut))
						{
							print "$neg\n";
						}
						else
						{
							print "$neut\n";
						}
					}
					$contpos = 0;
					$contneg = 0;		
					$contneut = 0;					
				}
				else
				{	
					print "$neut\n";
					#$cont3 += 1;								
				}
			#}
	}
	
	#my $cont = $cont1 + $cont2 + $cont3;
	#print OUT2 "\ntweets analisados: $cont\n";
	#print OUT2 "\ntotal tweets: $conttweet";
	
	&fechaArqs;
	
	sub fechaArqs
	{
		close IN;
		#close OUT;
		#close OUT2;
	}

