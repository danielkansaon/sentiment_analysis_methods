
import requests
from yandex.Translater import Translater
import pandas as pd

api_key = ''

def translate_text(text):
    # base = 'https://translate.yandex.net'
    # post = "/api/v1.5/tr.json/translate?lang=pt-en&text=%s&key=%s" %(text, api_key)

    # print('text: ', text)
    # request = requests.post(base + post)
    # print(post)
    # print(request)
    # translated_text = request.json()['text']
    # print('translated: ', translated_text[0])

    # return translated_text[0]

    tr = Translater()
    tr.set_key(api_key)
    tr.set_from_lang('pt')
    tr.set_to_lang('en')
    tr.set_text(text)

    print('text: ', text)
    result = tr.translate()
    print('translated: ', result)

    return result

def start_process(filename):
    intern_data = pd.read_csv('data/'+filename, sep='\t', header=0)
    target = intern_data['class']    
    text = intern_data['text']
    count = 0

    f = open('data/yandex_' + filename, "a+", encoding='utf-8')
    f.seek(0)
    start_pos = len(f.readlines())
    f.seek(start_pos)
    
    for i in range(start_pos, len(text)):
        f.write(translate_text(text[i]) + '\t' + str(target[i]) + '\n')
        count += 1
        print(count)

    f.close()

def main():                
    #start_process('dados_internos.tsv')
    start_process('dados_externos.tsv')

# if __name__ == '__main__':
#     main()
