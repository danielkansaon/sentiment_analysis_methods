## Details

This repository contains some sentiment analysis methods in Python, Java and Perl programming language.

Methods:

- emoticons
- panas
- emolex
- sent140
- NRC
- opinion lexicon
- sasa
- senticnet
- sentistrength
- stanford
- socal
- umigon
- afinn
- pattern
- vader
- opnionfinder


---

## Test

The file "test.py" contains instructions to test each method and return OK if the output is correct. The file must be executed with Python 2 (2.7.17)
