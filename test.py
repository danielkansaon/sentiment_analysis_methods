import unittest
import os
from subprocess import *
import sys

class TestSentimentAnalysisMethods(unittest.TestCase):
  def setUp(self):
    self.CURRENT_PATH = os.getcwd()

  def tearDown(self):
    os.chdir(self.CURRENT_PATH)


#   def test_file_v_afinn(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_afinn/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_afinn.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.6124\n-0.6124\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_afinn -f OK"

#   def test_text_v_afinn(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_afinn/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_afinn.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.6124')
#       out = check_output(sys.executable + " v_afinn.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.6124')  
#       print "v_afinn -t OK"

#   def test_file_v_emolex(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_emolex/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_emolex.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.25\n-0.25\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_emolex -f OK"

#   def test_text_v_emolex(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_emolex/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_emolex.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.25')
#       out = check_output(sys.executable + " v_emolex.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.25')  
#       print "v_emolex -t OK"  

#   def test_file_v_emoticonds(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_emoticonds/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you bad worse :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_emoticonds.py -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_emoticonds -f OK"

#   def test_text_v_emoticonds(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_emoticonds/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_emoticonds.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output(sys.executable + " v_emoticonds.py -t 'i hate you bad worse :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "v_emoticonds -t OK" 

#   def test_file_v_nrchashtag(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_nrchashtag/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you bad worse :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_nrchashtag.py -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_nrchashtag -f OK"

#   def test_text_v_nrchashtag(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_nrchashtag/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_nrchashtag.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output(sys.executable + " v_nrchashtag.py -t 'i hate you bad worse :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "v_nrchashtag -t OK"  

#   def test_file_v_opinionlexicon(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_opinionlexicon/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you bad worse :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_opinionlexicon.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.25\n-0.6124\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_opinionlexicon -f OK"

#   def test_text_v_opinionlexicon(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_opinionlexicon/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_opinionlexicon.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.25')
#       out = check_output(sys.executable + " v_opinionlexicon.py -t 'i hate you bad worse :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.6124')  
#       print "v_opinionlexicon -t OK"

#   def test_file_v_panas(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_panas/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i feel happy :)\ni am nervous  :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_panas.py -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_panas -f OK"

#   def test_text_v_panas(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_panas/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_panas.py -t 'i feel happy  :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output(sys.executable + " v_panas.py -t 'i am nervous :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "v_panas -t OK"

#   def test_file_v_sentiwordnet(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_sentiwordnet/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you  :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_sentiwordnet.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.1556\n-0.1901\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_sentiwordnet -f OK"

#   def test_text_v_sentiwordnet(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_sentiwordnet/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_sentiwordnet.py -t 'i love you  :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.1556')
#       out = check_output(sys.executable + " v_sentiwordnet.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.1901')  
#       print "v_sentiwordnet -t OK"

#   def test_file_v_sentiment140(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_sentiment140/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you  :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " v_sentiment140.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.4135\n-0.2903\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "v_sentiment140 -f OK"

#   def test_text_v_sentiment140(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/v_sentiment140/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " v_sentiment140.py -t 'i love you  :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.4135')
#       out = check_output(sys.executable + " v_sentiment140.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.2903')  
#       print "v_sentiment140 -t OK"

  def test_text_emoticon(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/emoticons/"
      os.chdir(method_dir)
      #Run method
      out = check_output("./emoticons.pl -t 'hi :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("./emoticons.pl -t 'hi :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')
      print "emoticons -t OK"    


  def test_file_emoticons(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/emoticons/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("./emoticons.pl -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "emoticons -f OK"    

  def test_text_panas(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/panas/"
      os.chdir(method_dir)
      #Run method
      out = check_output("./panas.pl -t 'that is surprise :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("./panas.pl -t 'i feel guilt :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')      
      print "panas -t OK"    

  def test_file_panas(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/panas/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("that is surprise :)\ni feel guilt :(\n")
      new_file.close()
      #Run method
      out = check_output("./panas.pl -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "panas -f OK"    

  def test_text_emolex(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/emolex/"
      os.chdir(method_dir)
      #Run method
      out = check_output("./emolex.pl -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("./emolex.pl -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "emolex -t OK"    

  def test_file_emolex(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/emolex/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("./emolex.pl -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "emolex -f OK"    

#   def test_text_happiness(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/happiness_index/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " happiness_index.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.93')
#       out = check_output(sys.executable  + " happiness_index.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.72')  
#       print "happiness_index -t OK"    

#   def test_file_happiness(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/happiness_index/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " happiness_index.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.93\n-0.72\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "happiness_index -f OK"    

  def test_text_sent140(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sentiment140/"
      os.chdir(method_dir)
      #Run method
      out = check_output("java -jar sentiment140.jar -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("java -jar sentiment140.jar -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "sentiment140 -t OK"    

  def test_file_sent140(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sentiment140/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("java -jar sentiment140.jar -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "sentiment140 -f OK"

  def test_text_NRC(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/NRC_Hashtag/"
      os.chdir(method_dir)
      #Run method
      out = check_output("java -jar nrc.jar -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1.458')
      out = check_output("java -jar nrc.jar -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-4.286')  
      print "NRC_Hashtag -t OK"    

  def test_file_NRC(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/NRC_Hashtag/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("java -jar nrc.jar -f example.txt",shell=True)
      self.assertEqual(out, '1.458\n-4.286\n')      
      #Remove file
      os.remove("example.txt")
      print "NRC_Hashtag -f OK"

  def test_text_OpinionLexicon(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/OpinionLexicon/"
      os.chdir(method_dir)
      #Run method
      out = check_output("./OpinionLexicon.pl -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("./OpinionLexicon.pl -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "OpinionLexicon -t OK"    

  def test_file_OpinionLexicon(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/OpinionLexicon/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("./OpinionLexicon.pl -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "OpinionLexicon -f OK"

  def test_text_sasa(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sasa/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " sasa_method.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '0.429883890943')
      out = check_output(sys.executable + " sasa_method.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-0.32763461365')  
      print "Sasa -t OK"    

  def test_file_sasa(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sasa/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " sasa_method.py -f example.txt",shell=True)
      self.assertEqual(out, '0.429883890943\n-0.32763461365\n')      
      #Remove file
      os.remove("example.txt")
      print "Sasa -f OK"

  def test_text_senticnet(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/senticnet/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " senticnet.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '0.333500')
      out = check_output(sys.executable + " senticnet.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-0.417500')  
      print "Senticnet -t OK"    

  def test_file_senticnet(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/senticnet/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " senticnet.py -f example.txt",shell=True)
      self.assertEqual(out, '0.333500\n-0.417500\n')      
      #Remove file
      os.remove("example.txt")
      print "Senticnet -f OK"

  def test_text_sentistrength(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sentistrength/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " sentistrength.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '2')
      out = check_output(sys.executable + " sentistrength.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-3')  
      print "Sentistrength -t OK"    

  def test_file_sentistrength(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/sentistrength/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " sentistrength.py -f example.txt",shell=True)
      self.assertEqual(out, '2\n-3\n')      
      #Remove file
      os.remove("example.txt")
      print "Sentistrength -f OK"

#   def test_text_sentiwordnet(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/sentiwordnet/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " sentiwordnet.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '0.61')
#       out = check_output(sys.executable + " sentiwordnet.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-0.75')  
#       print "Sentiwordnet -t OK"    

#   def test_file_sentiwordnet(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/sentiwordnet/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " sentiwordnet.py -f example.txt",shell=True)
#       self.assertEqual(out, '0.61\n-0.75\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "Sentiwordnet -f OK"

  def test_text_stanford(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/stanford_deep_model/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " stanford.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, 'Positive')
      out = check_output(sys.executable + " stanford.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, 'Negative')  
      print "Stanford Deep Model -t OK"    

  def test_file_stanford(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/stanford_deep_model/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " stanford.py -f example.txt",shell=True)
      self.assertEqual(out, 'Positive\nNegative\n')      
      #Remove file
      os.remove("example.txt")
      print "Stanford Deep Model -f OK"

  def test_text_socal(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/so-cal/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " so-cal.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '3.0')
      out = check_output(sys.executable + " so-cal.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-6.0')  
      print "SO-CAL -t OK"    

  def test_file_socal(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/so-cal/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " so-cal.py -f example.txt",shell=True)
      self.assertEqual(out, '3.0\n-6.0\n')      
      #Remove file
      os.remove("example.txt")
      print "SO-CAL -f OK"

  def test_text_umigon(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/Umigon/"
      os.chdir(method_dir)
      #Run method
      out = check_output("java -jar Umigon.jar -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("java -jar  Umigon.jar -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "Umigon -t OK"    

  def test_file_umigon(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/Umigon/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("java -jar Umigon.jar -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "Umigon -f OK"

#   def test_text_combined(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/combined_method/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " combined_method.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1.0')
#       out = check_output(sys.executable + " combined_method.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1.0')  
#       print "Combined Method -t OK"    

#   def test_file_combined(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/combined_method/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " combined_method.py -f example.txt",shell=True)
#       self.assertEqual(out, '1.0\n-1.0\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "Combined Method -f OK"


  def test_text_afinn(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/afinn/"
      os.chdir(method_dir)
      #Run method
      out = check_output("perl afinn.pl -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("perl afinn.pl -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "AFINN -t OK"    

  def test_file_afinn(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/afinn/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("perl afinn.pl -f example.txt",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "AFINN -f OK"

#   def test_text_aniko(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/aniko/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output("perl emoticons_aniko.pl -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output("perl emoticons_aniko.pl -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "aniko -t OK"    

#   def test_file_aniko(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/aniko/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output("perl emoticons_aniko.pl -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "aniko -f OK"

#   def test_text_aniko_python(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/aniko/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " aniko.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output(sys.executable + " aniko.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "aniko PYTHON -t OK"    

#   def test_file_aniko_python(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/aniko/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " aniko.py -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "aniko PYTHON -f OK"
 
  def test_text_pattern(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/pattern.en/"
      os.chdir(method_dir)
      #Run method
      out = check_output(sys.executable + " patt.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '0.5')
      out = check_output(sys.executable + " patt.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-0.775')  
      print "Pattern -t OK"    

  def test_file_pattern(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/pattern.en/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output(sys.executable + " patt.py -f example.txt",shell=True)
      self.assertEqual(out, '0.5\n-0.775\n')      
      #Remove file
      os.remove("example.txt")
      print "Pattern -f OK"

#   def test_text_sann(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/SANN/"
#       os.chdir(method_dir)
#       #Run method
#       out = check_output(sys.executable + " sentiment.py -t 'i love you :)'",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '1')
#       out = check_output(sys.executable + " sentiment.py -t 'i hate you :('",shell=True)
#       out = out.strip()
#       self.assertEqual(out, '-1')  
#       print "SANN -t OK"    

#   def test_file_sann(self):
#       #Go to method directory
#       method_dir = self.CURRENT_PATH + "/SANN/"
#       os.chdir(method_dir)
#       #Go create example input file
#       new_file = open("example.txt","w")
#       new_file.write("i love you :)\ni hate you :(\n")
#       new_file.close()
#       #Run method
#       out = check_output(sys.executable + " sentiment.py -f example.txt",shell=True)
#       self.assertEqual(out, '1\n-1\n')      
#       #Remove file
#       os.remove("example.txt")
#       print "SANN -f OK"

  def test_text_vader(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/vader/"
      os.chdir(method_dir)
      #Run method
      
      out = check_output("python3 vader.py -t 'i love you :)'",shell=True)
      out = out.strip()
      self.assertEqual(out, '0.802')
      out = check_output("python3 vader.py -t 'i hate you :('",shell=True)
      out = out.strip()
      self.assertEqual(out, '-0.765')  
      print "Vader -t OK"    

  def test_file_vader(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/vader/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("python3 vader.py -f example.txt", shell=True)
      print('valor: ', out)     
      self.assertEqual(out, '0.802\n-0.765\n') 
      #Remove file
      os.remove("example.txt")
      print "Vader -f OK"

  def test_text_opnionfinder(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/opnionfinder/"
      os.chdir(method_dir)
      #Run method
      out = check_output("java -jar MyOpinionFinder.jar -t 'i love you' 2> /dev/null",shell=True)
      out = out.strip()
      self.assertEqual(out, '1')
      out = check_output("java -jar MyOpinionFinder.jar -t 'i hate you' 2> /dev/null",shell=True)
      out = out.strip()
      self.assertEqual(out, '-1')  
      print "Opnion Finder -t OK"

  def test_file_opnionfinder(self):
      #Go to method directory
      method_dir = self.CURRENT_PATH + "/opnionfinder/"
      os.chdir(method_dir)
      #Go create example input file
      new_file = open("example.txt","w")
      new_file.write("i love you :)\ni hate you :(\n")
      new_file.close()
      #Run method
      out = check_output("java -jar MyOpinionFinder.jar -f example.txt 2> /dev/null",shell=True)
      self.assertEqual(out, '1\n-1\n')      
      #Remove file
      os.remove("example.txt")
      print "Opnion Finder -f OK"

if __name__ == '__main__':
    unittest.main()


