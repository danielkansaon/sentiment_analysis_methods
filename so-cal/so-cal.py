#############################################################################
#
# file_analyser.py 
# Apr 21th, 2015
# by Matheus Araujo
# 
#
# Usage:  python file_analyser.py input.txt output.txt
#
#############################################################################

import sys
import subprocess
import os
from optparse import OptionParser
from uuid import uuid4 as uuid

CURRENT_FILE_PATH = "/".join(os.path.abspath(__file__).split("/")[:-1])
if CURRENT_FILE_PATH != "":
	CURRENT_FILE_PATH += "/"
os.environ["PATH"] = os.environ["PATH"] + ":" + os.getcwd() + "/RULE_BASED_TAGGER_V1.14/Bin_and_Data/"

def runSOCAL(line):
	#########################################################################
	# PREPROCESSING
	#########################################################################
	#Generate unique name
	input_filename = CURRENT_FILE_PATH + "." + str(uuid()).replace("-","")
	tmp1_filename = CURRENT_FILE_PATH + "." + str(uuid()).replace("-","")
	tmp2_filename = CURRENT_FILE_PATH + "." + str(uuid()).replace("-","")
	tmp3_filename = CURRENT_FILE_PATH + "." + str(uuid()).replace("-","")

	line = line.replace("\n","")
#	subprocess.call("echo '" + line +"' > .input", shell=True)
	file_line = open(input_filename, "w")
	file_line.write(line)
	file_line.close()


	#Run Perl Scripts
	subprocess.call("perl " + CURRENT_FILE_PATH + "swapNOT.pl < "+ input_filename + " |\
				 perl " + CURRENT_FILE_PATH + "sentence_boundary.pl | \
				 perl " + CURRENT_FILE_PATH + "add_space.pl > " + tmp1_filename
	, shell=True)
	
	#Run Tagger
	os.chdir(os.environ["RULE_BASED_DIR"])
		
	subprocess.call("./tagger LEXICON.BROWN " + tmp1_filename + " BIGRAMS LEXICALRULEFILE.BROWN CONTEXTUALRULEFILE.BROWN > " + tmp2_filename,
	shell=True) 

	os.chdir(os.environ["CURRENT_DIR"])
	
	subprocess.call("perl " + CURRENT_FILE_PATH + "extract_vbg_ver2.p " + tmp2_filename + " " + tmp3_filename, shell=True)

	#########################################################################
	# SENTIMENT ANALYSIS based on SO_Run
	#########################################################################

	cutoff = 0.0
	config = "SO_Calc.ini"
	pos_mark = "y"
	neg_mark = "n"

	sentiment = subprocess.check_output("python  " + CURRENT_FILE_PATH + "SO_Calc.py " + CURRENT_FILE_PATH + config + " " + tmp3_filename + " " + CURRENT_FILE_PATH + ".output.txt " + CURRENT_FILE_PATH + ".richout.txt " + CURRENT_FILE_PATH + ".search.txt", shell=True)
	sentiment = float(sentiment)
	#print sentiment
	if sentiment > 0:
		print sentiment
	elif sentiment < 0:
		print sentiment
	else:
		print 0
	# Discrete out put
	# if sentiment > 0:
	# 	file_output.write("1\n")
	# elif sentiment < 0:
	# 	file_output.write("-1\n")
	# else:
	# 	file_output.write("0\n")

	#Clean directory for next input
	subprocess.call("rm " + input_filename,shell=True)
	subprocess.call("rm " + tmp1_filename,shell=True)
	subprocess.call("rm " + tmp2_filename,shell=True)
	subprocess.call("rm " + tmp3_filename,shell=True)

if __name__ == '__main__':
	#Set os environment
	os.environ["CURRENT_DIR"] = CURRENT_FILE_PATH
	os.environ["RULE_BASED_DIR"] = CURRENT_FILE_PATH + "/RULE_BASED_TAGGER_V1.14/Bin_and_Data/"
	parser = OptionParser()
	parser.add_option('-f', '--filename', dest='filename')
	parser.add_option('-t', '--text', dest='text')
	options, args = parser.parse_args()

	if len(sys.argv) != 3:
		print "Remeber the parameters: [-t|-f] ['text'|input_file]"
		#Clean Enviroment
		print "Finished"
		os.environ["CURRENT_DIR"] = ""
		os.environ["RULE_BASED_DIR"] = ""
		sys.exit(0)

	count = 0

	if options.filename:
		#Load input file and preprocess each line. I did this to be able to process long
		#input files
		with open(options.filename,"r") as file_obj:
			for line in file_obj:
				count += 1
				runSOCAL(line)
				
	if options.text:
		runSOCAL(options.text)

	#Clean Enviroment
	os.environ["CURRENT_DIR"] = ""
	os.environ["RULE_BASED_DIR"] = ""




